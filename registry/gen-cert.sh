#!/bin/sh

echo "When prompted for Common Name (e.g. server FQDN), use <ip addr>.xip.io"
read -p "Continue (please enter)" ARG
 
openssl req -newkey rsa:1024 -nodes -sha256 -keyout domain.key -x509 -days 1000 -out domain.crt

