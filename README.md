# kubectl

kubectl is the main program for interacting with the Kubernetes API. Download kubectl from the Kubernetes release artifact site with the curl tool.

# Clone the Repository

The following commands will clone a repository that contains a "Vagrantfile", which describes the set of virtual machines that will run Kubernetes on top of CoreOS.

```
git clone git@gitlab.com:sf-cloud/coreos-kubernetes.git
cd coreos-kubernetes
```

# Start the Machines

The default cluster configuration is to start a virtual machine for each role — master node, worker node, docker registry, and etcd server. 
Ensure the latest CoreOS vagrant image will be used by running ***vagrant box update***.

Then run ***vagrant up*** and wait for Vagrant to provision and boot the virtual machines.

# Configure Kubectl on your local machine

See https://coreos.com/kubernetes/docs/latest/kubernetes-on-vagrant.html#configure-kubectl

# Configure Docker on your local machine

A secured registry will run inside vm *registry* at address 172.17.4.10.xip.io:5000
You need to store the registry certificate in a new folder "172.17.4.10.xip.io:5000" inside /etc/docker/certs.d/.
Afterwards you need to login with user regi and password regi.

```
sudo mkdir -p /etc/docker/certs.d/172.17.4.10.xip.io:5000
sudo cp registry/domain.crt /etc/docker/certs.d/172.17.4.10.xip.io:5000/ca.crt
docker login -u regi -p regi 172.17.4.10.xip.io:5000
```